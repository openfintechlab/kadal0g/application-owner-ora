/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                          from "express";
import {AppHealth}                      from "../config/AppConfig";
import logger                           from "../utils/Logger";
import { v4 as uuidv4 }                 from "uuid";
import {PostRespBusinessObjects}        from "../mapping/bussObj/Response";
import util                             from "util";
import ApplicationOwnerValidator        from "../mapping/validators/ApplicationOwnerValidator";
import ApplicationOwnerController       from "../mapping/ApplicationOwnerController";


const router: any = express.Router();

router.get(`/`, async (request:any,response:any) => {
    let transid:string = uuidv4();
    logger.info(`Procedure called for fetching all application addresses`, `${transid}`);    
    response.set("Content-Type","application/json; charset=utf-8");        
    let app_id: string = (request.query.app_id)? request.query.app_id : undefined;
    let own_id: string = (request.query.own_id)? request.query.own_id : undefined;
    try{
        new ApplicationOwnerValidator(transid).validateParams({app_id: app_id, own_id: own_id});
        let resBO = await new ApplicationOwnerController(transid).getOnApp(app_id,own_id);
        response.status(200).send(resBO);
    }catch(error){
        logger.debug(`Error occured while getting application address`, `${transid}`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
        response.status(getErrRespCode(error));        
        if(error.metadata){            
            response.send(error);
        }else{                        
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    } 
});


router.post(`/`, async(request:any, response:any) => {    
    let transid:string = uuidv4();
    logger.debug(`${transid}: Calling http-post for posting data: ${request.data}`);
    response.set("Content-Type","application/json; charset=utf-8");     
    logger.info(`${transid}: Procedure called for creating application owner`);   
    try{
        let parsedObj = new ApplicationOwnerValidator(transid).validate(request.body);
        logger.debug(`${transid}: Parsed response from validation: ${util.inspect(parsedObj,{compact:true,colors:true, depth: null})}`);
        const resBO = await new ApplicationOwnerController(transid).create(parsedObj);
        response.status(201).send(resBO);
    }catch(error){
        logger.debug(`Error occured while creating application owner`, `${transid}`,  `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
        response.status(getErrRespCode(error));        
        if(error.metadata){            
            response.send(error);
        }else{                        
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    } 
});


router.put(`/`, async(request:any, response:any) => {
    let transid:string = uuidv4();
    logger.debug(`${transid}: Calling http-put for posting data: ${request.data}`, `PUT`);
    response.set("Content-Type","application/json; charset=utf-8");     
    logger.info(`Procedure called for creating application owner`, `${transid}`);   
    try{
        let parsedObj = new ApplicationOwnerValidator(transid).validate(request.body);
        logger.debug(`${transid}: Parsed response from validation: ${util.inspect(parsedObj,{compact:true,colors:true, depth: null})}`);
        const resBO = await new ApplicationOwnerController(transid).update(parsedObj);
        response.status(201).send(resBO);
    }catch(error){
        logger.debug(`Error occured while updating application owner`, `${transid}`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(getErrRespCode(error));        
        if(error.metadata){            
            response.send(error);
        }else{                        
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    } 
});



router.delete(`/`, async (request:any,response:any) => {
    let transid:string = uuidv4();
    logger.info(`${transid}: Procedure called for fetching all application addresses`);    
    response.set("Content-Type","application/json; charset=utf-8");        
    let app_id: string = (request.query.app_id)? request.query.app_id : undefined;
    let own_id: string = (request.query.own_id)? request.query.own_id : undefined;
    try{
        new ApplicationOwnerValidator(transid).validateParams({app_id: app_id, own_id: own_id});
        let resBO = await new ApplicationOwnerController(transid).delete(app_id,own_id);
        response.status(201).send(resBO);
    }catch(error){
        logger.debug(`${transid}: Error occured while creating application address:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(getErrRespCode(error));        
        if(error.metadata){            
            response.send(error);
        }else{                        
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    } 
    
});

function getErrRespCode(error:any): number{
    let respCode:number = 500;
    if(error.metadata === undefined){
        respCode = 500;
    }else if(error.metadata.status === '8153'){
        respCode= 404;
    }else{
        respCode = 500;
    }        
    return respCode;
}


/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
 router.get('/healthz',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded && !AppHealth.reload_required)
        res.status(200);    
    else{
        logger.error(`Health check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);
        res.status(503);    
    }
        
    res.send();
});


// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded)
        res.status(200);    
    else{
        logger.error(`Readiness check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);
        res.status(503);        
    }        
    res.send();
});


// Protect slow starting containers with startup probes
router.get('/startup',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded && AppHealth.express_loaded){
        res.status(200);    
    }else{
        logger.error(`Startup check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);        
        res.status(503);    
    }        
    res.send();
});


export default router;