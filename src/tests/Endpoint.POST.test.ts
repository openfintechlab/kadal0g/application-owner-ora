import util                     from "util";
import OracleDBInteractionUtil  from '../utils/OracleDBInteractionUtil';
import {Main}                   from "../index";
import AppConfig                from "../config/AppConfig";
import logger                   from "../utils/Logger";
import axios                    from "axios";
import chalk                    from "chalk";
import UTSetup from "./setup_express";



describe(`POST Endpoint Validation Test Cases`, () => {
    const post_message = require(`./POST.message.json`);    
    const delete_message = require(`./DELETE.message.json`);


    beforeAll(async () => {        
        try{
            logger.info(`Package Name`, `${AppConfig.package_name}`)
            await Main.start();            
            await OracleDBInteractionUtil.initPoolConnectionsWithConfig(); 
            
        }catch(error){                        
            logger.error(`Unable to proceed. Closing application`);       
            process.exit(1);
        }            
    });

    afterAll(async ()=> {
        // Delete specific record
        try{
            // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}/${delete_message.id}`
            const url = UTSetup.getURL() + `?app_id=${post_message["application-owner"].application_id}&own_id=${post_message["application-owner"].own_id}`;
            logger.info(`+ Cleaning created records using url: ${url}`);
            const response = await axios.delete( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
        }catch(error){            
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
            }else{
                logger.error(chalk.red(`Error while cleaning specific record`))
            }         
            fail(`Error while cleaning the records`);
        }
        await OracleDBInteractionUtil.closePoolAndExit(); 
        await Main.expressApp.server.close();
    });

    test(`POST ${AppConfig.package_name} to the backend service-plane`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}`
        const url = UTSetup.getURL();
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios({
                "method": "post",
                "url": url,
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "data": post_message
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).toBeTruthy();            
        }catch(error){
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).not.toBe(500);
            }
            fail(error.message)                         
        }        
    });

    test(`POST same ${AppConfig.package_name} to the backend service-plane. Expecting error code: 500`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}`
        const url = UTSetup.getURL();
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios({
                "method": "post",
                "url": url,
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "data": post_message
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201 ).not.toBeTruthy();            
        }catch(error){
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).toBe(500);
            }else{             
                fail(error.message)
            }            
        }        
    });

    test(`POST invalid JSON ${AppConfig.package_name} to the backend service-plane. Expecting error code: 500`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}`
        const url = UTSetup.getURL();
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        post_message.application_id = ""        
        try{    
            const response = await axios({
                "method": "post",
                "url": url,
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "data": post_message
            });
            const statusCode:number = response.status;
            expect(statusCode === 500).toBeTruthy();            
        }catch(error){
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).toBe(500);
            }else{                
                fail(error.message)                
            }            
        }        
    });
});