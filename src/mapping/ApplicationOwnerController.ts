
import OracleDBInteractionUtil          from "..//utils/OracleDBInteractionUtil";
import util                             from "util";
import { v4 as uuidv4 }                 from "uuid";
import  logger                          from "../utils/Logger";
import {PostRespBusinessObjects}        from "./bussObj/Response";

export default class ApplicationOwnerController{
    SQLStatements = {
        "SELECT_SQL01_ID_FROM_TBL": "SELECT own_id from APPLICATION_OWNER where emailAdd=:v1 AND mobile_no=:v2",
        "INSERT_SQL02_IN_APPOWN": "INSERT INTO application_owner ( application_id, own_id, emailadd, mobile_no, escalationrank, created_on, updated_on, created_by, updated_by ) VALUES ( :v0, :v1, :v2, :v3, :v4, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'SYSTEM', 'SYSTEM')",
        "SELECT_SQL03_ID_FROM_TBL": "SELECT own_id from APPLICATION_OWNER where application_id=:v1 AND own_id=:v2",
        "DELETE_SQL04_ID_FROM_TBL": "DELETE from APPLICATION_OWNER where application_id=:v1 AND own_id=:v2",
        "SELECT_SQL05_FROM_TBL_ONAPP": "SELECT application_id, own_id, emailadd, mobile_no, escalationrank from APPLICATION_OWNER where application_id=:v1 AND own_id=COALESCE(:v2, own_id)",
        "UPDATE_SQL06_FROM_TBL_ONAPP": "UPDATE application_owner SET    emailadd = :v1,    mobile_no = :v2,    escalationrank = :v3,        updated_on = CURRENT_TIMESTAMP,        updated_by = 'SYSTEM' WHERE    application_id = :v6    AND own_id = :v7"
    };

    constructor(private referenceID: string){}
    /**
     * Creates a new Application Owner
     * @param {any} request JSON request message 
     */
    public async create(request:any){
        logger.debug(`[${this.referenceID}]: Request Received for creating application-owner: ${util.inspect(request,{compact:true,colors:true, depth: null})}`);        
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL01_ID_FROM_TBL,[request['application-owner'].email_add, request['application-owner'].mobile_no]);
            if(result.rows.length > 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8152","Application already exist");
            }
            let binds = [
                request['application-owner'].application_id,
                request['application-owner'].own_id,
                request['application-owner'].email_add,
                request['application-owner'].mobile_no,
                request['application-owner'].escalationrank
            ]
            let dbResult = await OracleDBInteractionUtil.executeUpdate(this.SQLStatements.INSERT_SQL02_IN_APPOWN,binds);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        }catch(error){
            logger.error(`${this.referenceID} Error adding application in the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    /**
     * UPDATES application owner
     * @param {any} request JSON request message 
     */
    public async update(request:any){
        logger.debug(`[${this.referenceID}]: Request Received for creating application-owner: ${util.inspect(request,{compact:true,colors:true, depth: null})}`);        
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL03_ID_FROM_TBL,[request['application-owner'].application_id, request['application-owner'].own_id]);
            if(result.rows.length <= 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db"); 
            }
            let binds = [                
                request['application-owner'].email_add,
                request['application-owner'].mobile_no,
                request['application-owner'].escalationrank,
                request['application-owner'].application_id,
                request['application-owner'].own_id
            ]
            let dbResult = await OracleDBInteractionUtil.executeUpdate(this.SQLStatements.UPDATE_SQL06_FROM_TBL_ONAPP,binds);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        }catch(error){
            logger.error(`${this.referenceID} Error adding application in the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    /**
     * Deletes a specific application's owner from the database
     * @param {string} app_id Application ID
     * @param {string} own_id Owner ID
     */
    public async delete(app_id: string, own_id: string){
        try{
            logger.debug(`[${this.referenceID}]: Request Received for creating application-owner: app_id: ${app_id} own_id: ${own_id}`);        
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL03_ID_FROM_TBL,[app_id,own_id]);
            if(result.rows.length <= 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db"); 
            }
            let dbResult = await OracleDBInteractionUtil.executeUpdate(this.SQLStatements.DELETE_SQL04_ID_FROM_TBL,[app_id,own_id]);
            if(dbResult.rowsAffected > 0){
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");   
            }                
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db");   
            }
        }catch(error){
            logger.error(`${this.referenceID} Error adding application in the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    /**
     * Get Application based on the app_id and owner id
     * @param {string} app_id Application ID
     * @param {string} own_id Owner ID
     */
    public async getOnApp(app_id:string, own_id?:string){
        try{
            let dbResult = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL05_FROM_TBL_ONAPP,[app_id,own_id]);
            return await this.createBusinessObject(dbResult);
        }catch(error){
            logger.error(`${this.referenceID} Error adding application in the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    private async createBusinessObject(dbResult:any){
        return new Promise<any>((resolve:any, reject:any)=> {
            if(dbResult.rows.length <= 0){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db"));
            }
            var bussObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),                    
                },
                "application-owner": []
            }
            dbResult.rows.forEach((row:any) => {
                bussObj["application-owner"].push({
                    "application_id": row.APPLICATION_ID,
                    "own_id": row.OWN_ID,
                    "email_add":row.EMAILADD,
                    "mobile_no": row.MOBILE_NO,
                    "escalationrank": row.ESCALATIONRANK
                });
            });
            resolve(bussObj);
        });
    }
}