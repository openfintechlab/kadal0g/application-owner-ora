import Joi                              from "joi";
import logger                           from "../../utils/Logger"
import {PostRespBusinessObjects}        from "../bussObj/Response";
import AppConfig                        from "../../config/AppConfig";

export default class ApplicationOwnerValidator{

    constructor(private referenceID: string){

    }

    ApplicationOwnerSchema = Joi.object({
        "application-owner": {
            "application_id": Joi.string().min(8).max(56).required(),
            "own_id":  Joi.string().min(8).max(56).required(),
            "email_add": Joi.string().email().required(),
            //https://regexr.com/3c53v
            "mobile_no": Joi.string().regex(new RegExp(`^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$`)).required(),
            "escalationrank": Joi.number().min(1).max(10).required()
        }        
    });

    ApplicationOwnerParamScehma = Joi.object({
        "app_id": Joi.string().min(8).max(56).required(),
        "own_id": Joi.string().min(8).max(56).optional()
    });

    public validate(request: any){
        let {error, value} = this.ApplicationOwnerSchema.validate(request, { presence: "required" } );
        if(error){
            logger.error(`[${this.referenceID}]: Error while validating the business object: ${error.message}`);
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    public validateParams(request:any){
        let {error,value} = this.ApplicationOwnerParamScehma.validate(request, {presence: "required"});
        if(error){
            logger.error(`[${this.referenceID}]: Error while validating the business object: ${error.message}`);
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    private getValidationError(error:any){
        if(AppConfig.config.OFL_MED_NODE_ENV === 'debug'){
            let trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8502","Schema validation error", trace);   
        }else{
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8502","Schema validation error");   
        }           
    }
}

/**
 * CREATE TABLE application_owner (
    application_id  VARCHAR2(56 CHAR) NOT NULL,
    own_id          VARCHAR2(64 CHAR) NOT NULL,
    emailadd        VARCHAR2(128 CHAR) NOT NULL,
    mobile_no       VARCHAR2(15 CHAR) NOT NULL,
    escalationrank  NUMBER NOT NULL,
    created_on      TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    updated_on      TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    created_by      VARCHAR2(64 CHAR) NOT NULL,
    updated_by      VARCHAR2(64 CHAR)
);

 */