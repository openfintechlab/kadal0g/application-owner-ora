/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Part of framework
 * Description:
 * Module for loading configuration from .env file
 * Ref: https://github.com/motdotla/dotenv/tree/master/examples/typescript
 */
import axios        from "axios";
import logger       from "../utils/Logger";
import util         from "util";
import chalk                                    from "chalk";
import * as _                                   from "lodash";


const package_json = require(`../../package.json`);
const node_package_name = package_json.name;
const node_package_version = package_json.version;

/**
 * Class for encapstulating Application Configuration
 */
export default abstract class AppConfig {
 

    /* START: MANDATORY Variables */
    private static cfgContextRoot: string | null; // Context root of the service component. By default it will be set as a package name
    private static cfgServiceConfigURI: string | null; // Service configuration URI
    private static cfgPackageName: string | null; // Service package name. Fetched from package.json
    private static cfgServiceLoadInterval: number; // Pooling interval for loading service configuration from coinfiguration manager service component
    private static cfgObject: any; // Stores config.<ServiceName>[].*
    /* END */

    /**
     * Get Context Root of service component. 
     * <b>NOTE:</b> By default, context root will always be the package name set in package.json
     */
    public static get contextRoot(): string{        
        if(!this.isDefined(this.cfgContextRoot) || AppHealth.reload_required){
            this.cfgContextRoot = '/' + node_package_name;
        }   
        return this.cfgContextRoot as string;        
    }

    /**
     * Get Service Configuration service URL
     * <b>NOTE:</b> Values will be loaded from environment variable OFL_MED_SRVCONFIG_URI OR from service call (same variable name for service config)
     */
    public static get service_config_uri(): string {
        if(!this.isDefined(this.cfgServiceConfigURI) && !this.isDefined(process.env.OFL_MED_SRVCONFIG_URI)){
            throw Error("Fatal Error! URL for fetching service configuration is not set properly");
        }else if(!this.isDefined(this.cfgServiceConfigURI) || AppHealth.reload_required){            
            this.cfgServiceConfigURI = (!this.isDefined(this.cfgObject))? process.env.OFL_MED_SRVCONFIG_URI as string : this.cfgObject.OFL_MED_SRVCONFIG_URI;                     
        }        
        return this.cfgServiceConfigURI as string;
    }

    /**
     * Get package name from pacakge.json
     */
    public static get package_name(): string{        
        const packageName:string = package_json.name;
        if((!this.isDefined(packageName) || packageName.length <= 0) && !this.isDefined(this.cfgPackageName)){
            throw Error("Fatal Error! Package name can not be null");
        }        
        this.cfgPackageName = packageName;
        return this.cfgPackageName as string;
    }

    /**
     * Get Configuration Loading Pooling Interval
     */
    public static get service_load_interval(): number{
        this.cfgServiceLoadInterval = (process.env.OFL_MED_CFGPOOLINT || 15 * 1000) as number;        
        return (this.cfgServiceLoadInterval as number);        
    }

    /**
     * Get service config JS object
     */
    public static get config(): any{
        return this.cfgObject;
    }

    /**
     * Set service configuration with js object
     */
    public static set config(obj:any){
        this.cfgObject = obj;
    }

    /**
     * Set service config URL
     */
    public static set service_config_uri(uri: string){
        this.cfgServiceConfigURI = uri;
    }

    /**
     * Set package name
     */
    public static set package_name(pkg:string){
        this.cfgPackageName = pkg;
    }
    
    /**
     * Procedure for checking value of the object
     * @param obj object to check value of
     */
    private static isDefined(obj:any): boolean{
        return !(obj === undefined || obj === null);
    }

    
    /**
     * Loads configuration from configuration service component
     * @param uri URL of service configuration Service Component
     * @param servicename Name of the service / package. As a standard, we are using package name as service name
     */
    public static async loadConfigurationfromConfigService(uri:string, servicename: string){
        logger.debug("Getting configuration from the config service")   
        this.displayConfigEndpointDetails();
        let cfgURL = uri+"SRVCFG."+servicename + "." + node_package_version;
        logger.debug(`Fetching configuration using url: ${cfgURL}`);
        const url = new URL(cfgURL);   
        logger.debug(`Getting configuration from url: ${url.toString()}`) 
        try{
            const response = await axios.get(url.toString());                
            logger.debug(`Response Received (supressing response message) ${util.inspect(response.data,{compact:true,colors:true, depth: null})}`);
            // iterate through the JSON and populate environment                    
            if( !this.isDefined(response.data.metadata)|| 
                response.data.metadata.status !== '0000' ||
                !this.isDefined(response.data.config) ||
                response.data.config[servicename] === undefined // || 
                /*response.data.config[servicename][0] === undefined*/ ){
                logger.fatal(`Error while validating the configuration response. Please validate the message structure serviceName: ${servicename}`);
                logger.error(`Error Response Received setting health metric from ${AppHealth.config_loaded} TO false`);
                AppHealth.config_loaded = false;
                throw new Error("Error received while calling configuration store");
            }         
            logger.debug(`+ Response validated`);
            const jsonObj = response.data.config[servicename];  
            if(!this.isDefined(this.cfgObject)){
                this.cfgObject = jsonObj; // Load only when the object is not defined
            }else{                
                const appReload = !_.isEqual(this.cfgObject,jsonObj);
                AppHealth.reload_required = (AppHealth.reload_required)? true : appReload; // Reload flag will change only when it is set as false.
                logger.debug(`Setting Reload flag to value: ${AppHealth.reload_required}`);
                this.cfgObject = jsonObj;
            }            
            const configCount:number = Object.keys(jsonObj).length;                                
            if(configCount <= 0 ){    
                AppHealth.config_loaded = false;    
                logger.error("No configuration key recevied from CACHE.")
            }else{
                AppHealth.config_loaded = true;
                logger.debug("Count of config keys serialize in environment = "+configCount);
                this.resetLogLevel();
            }
            return response.data;
        }catch(error){
            AppHealth.config_loaded = false;        
            logger.debug(`Error Occured while calling the ConfigurationManager service: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);                
            throw error;
        }
    }    

    /**
     * Start Pooling agent for loading service configurations
     */
    public static async startConfigLoaderPoolAgent(){
        logger.info(`Starting Pooling agent for loading configurations with pooling interval: ${AppConfig.service_load_interval}`);
        setInterval(async() => {
            try{
               await this.loadConfigurationfromConfigService(AppConfig.service_config_uri, AppConfig.package_name);                
            }catch(error){
                logger.error(`Error occured while bootloading the solution`);                
            }            
        }, AppConfig.service_load_interval);
    }


    private static displayConfigEndpointDetails(){             
        logger.debug(`Configuration Manager URL: ` + chalk.underline.cyan.bold(`${AppConfig.service_config_uri}`));
        logger.debug(`Package Name: ` + chalk.underline.cyan.bold(`${AppConfig.package_name}`));        
    }

    private static resetLogLevel(){
        logger.level = AppConfig.config.OFL_MED_NODE_ENV;         
        logger.debug(`Setting Log Level To: ` + chalk.underline.cyan.bold(`${logger.level}`));               
    }
   
    
}


export const AppHealth = {
    "config_loaded": false as boolean,
    "express_loaded": false as boolean,
    "config_uri_loaded": false as boolean,
    "reload_required": false as boolean
}