/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Logger utility class
 */
import log4js       from "log4js";
import AppConfig    from "../config/AppConfig"; 


const logger = log4js.getLogger();
// Change to AppConfig.config.NNASA
// logger.level = (AppConfig === undefined )?'debug': AppConfig.log_level;        
logger.level = (AppConfig === undefined )? process.env.OFL_MED_NODE_ENV || 'info' : (AppConfig.config === undefined) ? 'info': AppConfig.config.OFL_MED_NODE_ENV;        
logger.debug('Logger initiated...');

export default logger;